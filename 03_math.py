#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
"""
import os
import csv
import math
import random
import tkinter as tk
import tkinter.font as font

from copy import deepcopy
from timeit import default_timer as timer

import config
from utils.circular_buffer import CircularBuffer


class User:
    def __init__(self):
        self.name = ""

        # List containing (id, response_time, math_task, correct_answer,
        #                  user_answer, correct) for each trial
        self.trial_results = []

        # List containing (id, [(time, x, y, velocity), ...]) for each trial
        self.mouse_movement = []

    def save_trial_results(self):
        # Ensure the results folder exists
        if not os.path.isdir(config.setup_03_math["results"]):
            os.mkdir(config.setup_03_math["results"])

        # Save the response times
        with open(f"{config.setup_03_math['results']}/{self.name}.csv", mode="w") as f:
            csv_writer = csv.writer(f)
            # Write header
            csv_writer.writerow(
                ["Participant", "Trial", "Response time [s]", "Math task", "Correct answer", "Answer", "Correct"]
            )

            for row in self.trial_results:
                csv_writer.writerow([self.name] + row)

    def save_mouse_movement(self):
        # Ensure the results folder exists
        if not os.path.isdir(config.setup_03_math["results"]):
            os.mkdir(config.setup_03_math["results"])

        # Save the mouse movement
        with open(f"{config.setup_03_math['results']}/{self.name}.mouse.csv", mode="w") as f:
            csv_writer = csv.writer(f)
            csv_writer.writerow(
                ["Participant", "Trial", "Time [s]", "x [px]", "y [px]", "Velocity [px/s]", "Acceleration [px/s^2]"]
            )
            for id, data in self.mouse_movement:
                for row in data:
                    csv_writer.writerow([self.name, id] + row)


class MathTask:
    def __init__(self, root, x: int, y: int):
        self.root = root

        # Text containing the math expression
        params = deepcopy(config.info_text)
        params["font"] = config.setup_03_math["text"]["font"]
        self.text = tk.Text(self.root, width=7, height=1, **params)
        # Disable editing
        self.text.configure(state="disabled")

        # The numbers from which the task is constructed
        self.first_number = ""
        self.second_number = ""
        self.sign = ""

        # Position of the tast on the screen
        self.x = x
        self.y = y
        self.move_to(x, y)

    def update_task(self, first_number: int = None, second_number: int = None, sign: str = None):
        if first_number is not None:
            self.first_number = first_number
        if second_number is not None:
            self.second_number = second_number
        if sign is not None:
            self.sign = sign

        # Allow editing
        self.text.configure(state="normal")
        self.text.delete("1.0", tk.END)
        self.text.insert("1.0", f"{self.first_number} {self.sign} {self.second_number}")

        self.text.tag_add("all", "1.0", tk.END)

        # Disable editing
        self.text.configure(state="disabled")

    def check_answer(self, first_digit: str) -> bool:
        return self.get_correct_answer() == first_digit

    def move_to(self, x, y):
        self.x = x
        self.y = y
        self.text.place(x=x, y=y, anchor=tk.CENTER)

    def show(self):
        self.text.place(x=self.x, y=self.y, anchor=tk.CENTER)

    def hide(self):
        self.text.place_forget()

    def change_color(self, color: str):
        self.text.tag_config("all", foreground=config.setup_03_math["text"]["color"][color])

    def set_contrast(self, contrast: float):
        """
        Set the contrast value to a value between 'low_contrast' and 'high_contrast'
        from config.py. The resulting value in a linear interpolation
        between low and high

        'constrast' should be float in the range (0, 1)
            0 - 'low_contrast'
            1 - 'high_contrast'

        """
        value = (1 - contrast) * config.setup_03_math["text"]["color"][
            "low_contrast"
        ] + contrast * config.setup_03_math["text"]["color"]["high_contrast"]
        h = hex(int(value))[2:].zfill(2)
        self.text.tag_config("all", foreground=f"#{h}{h}{h}")

    def get_text(self):
        return f"{self.first_number} {self.sign} {self.second_number}"

    def get_correct_answer(self):
        if self.sign == "+":
            return str(self.first_number + self.second_number)[0]
        if self.sign == "-":
            return str(self.first_number - self.second_number)[0]


class App:
    def __init__(self):
        self.current_input = ""

        self.last_mouse_pos = None
        # List containing (time, x, y, velocity, acceleration) for the current trial
        self.mouse_movement_history = []

        # Average velocity to filter out drops & peaks
        self.velocity_data = CircularBuffer(config.setup_03_math["averaging_window_len"], default=0)
        self.acceleration_data = CircularBuffer(config.setup_03_math["averaging_window_len"], default=0)

        self.mouse_velocity = 0
        self.mouse_acceleration = 0

        # Green - fast enough, red - too slow
        self.velocity_square = None

        # Mean mouse position from the last trial - determines the difficulty of the next task
        self.last_trial_mean = [None, None]

        self.experiment_running = False
        self.trial_running = False
        self.trial_id = 0

        # Class representing the current user
        # (holds name and response times)
        self.current_user = User()

        # Used to measure time required to complete the trial
        self.trial_start = None

    def run(self):
        # Create the window environment
        self.root = tk.Tk(className="03_math")
        self.root["background"] = config.window["color"]["background"]
        # .geometry requires window size as a string "WxH"
        if config.window["size"]["fullscreen"]:
            width = self.root.winfo_screenwidth()
            height = self.root.winfo_screenheight()
        else:
            width = config.window["size"]["width"]
            height = config.window["size"]["height"]

        self.root.geometry(f"{width}x{height}")

        # Create the main canvas
        self.canvas = tk.Canvas(self.root, bg=config.window["color"]["background"], highlightthickness=0)
        self.canvas.pack(fill="both", expand=True)

        # Object that holds the terms and sign which make up the math task
        self.math_task = MathTask(self.root, width // 2, height // 2)

        # Prepare entry box
        my_font = font.Font(font=config.info_text["font"])
        self.name_entry = tk.Entry(self.root)
        self.name_entry_window = self.canvas.create_window(width // 2, height // 2 - 40, window=self.name_entry)
        self.name_entry["font"] = my_font
        self.name_entry.focus()

        # Shows information and instructions in the upper left corner
        self.info_text = tk.Text(self.root, width=39, pady=10, height=1, **config.info_text)

        self.info_text.place(x=2, y=0, anchor=tk.NW)
        self.info_text.insert("1.0", "Enter your name and press <Enter> to start")

        # Shows e.g. "Trial 2/10" in the upper left corner
        self.trial_id_text = tk.Text(self.root, width=12, pady=5, height=1, **config.info_text)

        self.trial_id_text.place(x=2, y=35, anchor=tk.NW)

        # Start the function responsible for measuring the mouse velocity
        self.measure_mouse_velocity()

        # Start the function responsible for moving the text based on mouse velocity
        self.update_task()

        self.root.bind("<KeyPress>", self.key_press)

        self.root.bind("<Escape>", lambda _: self.close_window())
        self.root.protocol("WM_DELETE_WINDOW", self.close_window)
        self.root.mainloop()

    def key_press(self, e):
        # First detect special cases
        # Space (start trial)
        if e.char == "\r":
            if not self.experiment_running and self.trial_id == 0:
                self.start_experiment()
            return

        if e.char == " ":
            if not self.trial_running and self.trial_id == 0:
                self.start_trial()
            return

        # Then filter everything which is not a number
        if not str.isdigit(e.char):
            return

        if self.trial_start is None:
            return
        # Check answer and end the trial
        correct = self.math_task.check_answer(e.char)
        self.end_trial(True, correct, e.char, timer() - self.trial_start)

    def measure_mouse_velocity(self):
        x = self.root.winfo_pointerx()
        y = self.root.winfo_pointery()

        if self.last_mouse_pos is not None:
            # Velocity is calculated as the euclidean distance between
            # current pos and last pos
            self.velocity_data.push(math.sqrt((self.last_mouse_pos[0] - x) ** 2 + (self.last_mouse_pos[1] - y) ** 2))

        self.acceleration_data.push(abs(self.velocity_data[0] - self.velocity_data[1]))

        # Calculate average of the last data
        self.mouse_velocity = sum(self.velocity_data.get_all()) / self.velocity_data.capacity
        self.mouse_acceleration = sum(self.acceleration_data.get_all()) / self.acceleration_data.capacity

        # Convert to [px/s]
        self.mouse_velocity *= 1000 / config.setup_03_math["update_period_ms"]
        self.mouse_acceleration *= 1000 / config.setup_03_math["update_period_ms"]

        # Change the color of the square based on the mouse velocity
        if self.mouse_velocity >= config.setup_03_math["minimal_mouse_velocity"]:
            color = config.setup_03_math["text"]["color"]["right"]
        else:
            color = config.setup_03_math["text"]["color"]["wrong"]

        if self.velocity_square is not None:
            self.canvas.itemconfigure(self.velocity_square, fill=color, outline=color)

        self.last_mouse_pos = (x, y)

        # Save the mouse info and end the task if the mouse velocity drops
        # under the specified threshold
        if self.experiment_running and self.trial_running:
            self.mouse_movement_history.append(
                [timer() - self.trial_start, x, y, self.mouse_velocity, self.mouse_acceleration]
            )
            if self.mouse_velocity < config.setup_03_math["minimal_mouse_velocity"]:
                self.end_trial(False, False, "", 0)

        # Reschedule the function to run again
        # This is an effective way to ensure the function is run periodically
        # inside the Tkinter environment
        self.root.after(config.setup_03_math["update_period_ms"], self.measure_mouse_velocity)

    def update_task(self):
        if self.experiment_running and self.trial_running:
            # math.atan is used to map all values to the [0, 1) range
            # (since the values are positive, the output range is [0, pi/2),
            # by dividing by pi/2, we get output range [0, 1)
            # Using atan removes the need to clip the values at some
            # arbitrarily high value, instead it just returns values
            # close to pi/2 (1 after normalization)

            # The constants 5000 are used so that the regular values of
            # mouse_velocity and mouse_acceleration (that are also around the
            # 5000 value) are mapped to a region around 1, where atan is
            # also linear - thus ensuring the contrast/jitter changes
            # "linearly" with change in velocity/acceleration
            # contrast = math.atan(self.mouse_velocity / 5000) / (math.pi / 2)
            contrast = min((self.mouse_velocity / 3000) ** 4, 1)
            self.math_task.set_contrast(contrast)

            # jitter_rate = math.atan(self.mouse_acceleration / 5000) / (math.pi / 2)
            jitter_rate = min((self.mouse_acceleration / 3000) ** (3), 1)

            # Jitter is modeled as a gaussian noise around the origin,
            # with variance given by jitter_rate (*30 since rates in the range
            # (0, 1) resulted in a really small jitter)
            self.math_task.move_to(
                random.gauss(self.width // 2, jitter_rate * 400), random.gauss(self.height // 2, jitter_rate * 400)
            )

        else:
            self.math_task.move_to(self.width // 2, self.height // 2)

        # Reschedule the function to run again after one second
        # This is an effective way to ensure the function is run periodically
        # inside the Tkinter environment
        self.root.after(50, self.update_task)

    def show_instructions(self):
        self.instruction_square = self.canvas.create_rectangle(
            self.width // 2 - 200,
            self.height // 8 - 60,
            self.width // 2 + 200,
            self.height // 8 + 140,
            outline="#d3d7cf",
            fill="#d3d7cf",
        )

        self.velocity_square = self.canvas.create_rectangle(
            self.width // 2 - 30,
            self.height // 8 - 30,
            self.width // 2 + 30,
            self.height // 8 + 30,
            outline="#e04854",
            fill="#e04854",
        )

        params = deepcopy(config.info_text)
        params["background"] = config.info_text["foreground"]
        params["foreground"] = "#222222"

        self.instruction_text = tk.Text(self.canvas, width=30, height=2, wrap=tk.WORD, **params)

        self.instruction_text.insert("1.0", "Please start moving the mouse until the square gets green.")
        self.instruction_text.place(x=self.width // 2, y=self.height // 8 + 85, anchor=tk.CENTER)

    def hide_instructions(self):
        self.instruction_text.place_forget()
        self.canvas.itemconfigure(self.instruction_square, state="hidden")

    def start_experiment(self):
        name = self.name_entry.get()

        # Do not proceed when the name is empty
        if name == "":
            return

        self.current_user.name = name

        self.experiment_running = True

        # Hide the entry box
        self.canvas.delete(self.name_entry_window)

        # Hide the start button
        # self.start_button.place_forget()

        self.info_text.delete("1.0", tk.END)
        self.info_text.insert("1.0", "Press <spacebar> to start the first trial")

        # Show instructions for 3 seconds and then start the first trial
        self.show_instructions()
        self.root.after(3000, self.hide_instructions)

    def end_experiment(self):
        # Save the response times to csv
        self.current_user.save_trial_results()
        self.current_user.save_mouse_movement()

        # End the experiment so that mouse movement does not move cursor
        self.experiment_running = False

        # Show the mouse cursor
        self.root.config(cursor="arrow")

        self.trial_id_text.delete("1.0", tk.END)
        self.trial_id_text.insert("1.0", "Done!")

        self.info_text.delete("1.0", tk.END)
        self.info_text.insert("1.0", "Press <esc> or close the window to exit")

    def start_trial(self):
        if not (self.experiment_running and not self.trial_running):
            return

        self.trial_id += 1
        self.trial_id_text.delete("1.0", tk.END)
        self.trial_id_text.insert("1.0", f"Trial {self.trial_id}/{config.setup_03_math['trials']}")

        self.info_text.delete("1.0", tk.END)

        # Reset the task
        self.current_input = ""

        # Choose data for the next task
        a = b = 0
        sign = "+"
        restrict_last_digit = False

        # Choose sign based on the average X position
        if self.last_trial_mean[0] is not None and self.last_trial_mean[0] < self.width // 2:
            sign = "-"

        # Restrict second digit based on the average Y position
        if self.last_trial_mean[1] is not None and self.last_trial_mean[1] < self.height // 2:
            restrict_last_digit = True

        # Find two 2-digit numbers, such that both their sum and difference
        # has also 2 digits
        while not (
            len(str(a)) == 2
            and len(str(b)) == 2
            and len(str(a + b)) == 2
            and len(str(a - b)) == 2
            and a - b > 0
            and (not restrict_last_digit or (int(str(a)[1]) < 5 and int(str(b)[1]) < 5))
        ):
            # Generating random number until the conditions are met is
            # easier :)
            a = random.randint(20, 89)
            b = random.randint(10, 79)

        # Generate the task
        self.math_task.update_task(a, b, sign)
        self.math_task.set_contrast(0)

        self.trial_running = True

        self.trial_start = timer()

    def end_trial(self, valid, correct, users_answer, response_time):
        if not (self.experiment_running and self.trial_running):
            return
        self.trial_running = False

        if valid:
            res = [
                self.trial_id,
                response_time,
                self.math_task.get_text(),
                self.math_task.get_correct_answer(),
                users_answer,
                int(correct),
            ]
            self.current_user.trial_results.append(res)
            self.current_user.mouse_movement.append([self.trial_id, self.mouse_movement_history])
        else:
            self.trial_id -= 1

        if not valid:
            self.math_task.change_color("invalid")
        else:
            if correct:
                self.math_task.change_color("right")
            else:
                self.math_task.change_color("wrong")

        # Calculate mean X and Y values, which will determine the difficulty
        # of the next task
        # Set the begin values to actual position to avoid issues when no data is available
        self.last_trial_mean = [self.root.winfo_pointerx(), self.root.winfo_pointery()]

        for _, x, y, _, _ in self.mouse_movement_history:
            self.last_trial_mean[0] += x
            self.last_trial_mean[1] += y

        if len(self.mouse_movement_history):
            self.last_trial_mean[0] /= len(self.mouse_movement_history)
            self.last_trial_mean[1] /= len(self.mouse_movement_history)

        self.mouse_movement_history = []

        if self.trial_id < config.setup_03_math["trials"]:
            self.info_text.insert("1.0", "Next trial will start automatically")

            if not valid:
                self.info_text.insert("1.0", "Too slow! ")

            # Pause for a while before starting the next trial
            self.root.after(100, self.start_trial)

        else:
            self.end_experiment()

    def close_window(self):
        self.root.quit()
        self.root.destroy()

    @property
    def width(self):
        return self.root.winfo_width()

    @property
    def height(self):
        return self.root.winfo_height()


if __name__ == "__main__":
    app = App()
    app.run()
