#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
"""
import matplotlib

import numpy as np
import tkinter as tk
import matplotlib.pyplot as plt

from timeit import default_timer as timer
from scipy.interpolate import make_interp_spline
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg

import config
from utils.circular_buffer import CircularBuffer

matplotlib.use("TkAgg")


class App:
    def __init__(self):
        # The X position of mouse is collected over one second, then average
        # position, velocity and acceleration are computed from the data
        self.position_data = []

        # Create data containers to hold the measured data history
        self.position = CircularBuffer(config.setup_02_mouse["data_len"], 0)
        self.velocity = CircularBuffer(config.setup_02_mouse["data_len"], 0)
        self.acceleration = CircularBuffer(config.setup_02_mouse["data_len"], 0)

        # X axis stays the same, create it now for convenience
        self.x = list(range(config.setup_02_mouse["data_len"]))
        self.x_smooth = np.linspace(0, len(self.x) - 1, 4 * len(self.x), endpoint=True)

        self.submoves = 4
        self.timer = timer()

    def run(self):
        # Create the window environment
        self.root = tk.Tk(className="02_mouse")
        self.root["background"] = config.window["color"]["background"]
        # .geometry requires window size as a string "WxH"
        if config.window["size"]["fullscreen"]:
            self.width = self.root.winfo_screenwidth()
            self.height = self.root.winfo_screenheight()
        else:
            self.width = config.window["size"]["width"]
            self.height = config.window["size"]["height"]

        self.root.geometry(f"{self.width}x{self.height}")
        # self.root.attributes("-fullscreen", True)
        # Create the main frame and setup the graph
        self.frame = tk.Frame(self.root, background=config.window["color"]["background"])
        self.frame.pack(side=tk.LEFT, fill=tk.BOTH, expand=1)

        self.fig, self.axes = plt.subplots(nrows=3, ncols=1)
        self.fig.set_facecolor(config.plot_colors["background"])
        plt.ion()

        for ax in self.axes:
            ax.set_facecolor(config.plot_colors["background"])

            # ax.xaxis.label.set_color(config.plot_colors["spines"])
            # ax.yaxis.label.set_color(config.plot_colors["spines"])
            # ax.tick_params(axis='x', colors=config.plot_colors["tickers"])
            # ax.tick_params(axis='y', colors=config.plot_colors["tickers"])
            ax.xaxis.set_visible(False)
            ax.yaxis.set_visible(False)

            if config.setup_02_mouse["absolute_values"]:
                ax.spines["bottom"].set_color(config.plot_colors["spines"])
            else:
                ax.spines["bottom"].set_color(config.plot_colors["background"])

            ax.spines["left"].set_color(config.plot_colors["background"])
            ax.spines["top"].set_color(config.plot_colors["background"])
            ax.spines["right"].set_color(config.plot_colors["background"])

        canvas = FigureCanvasTkAgg(self.fig, master=self.frame)
        plot_widget = canvas.get_tk_widget()
        plot_widget.pack(side=tk.TOP, fill=tk.BOTH, expand=1, padx=(0, 0), pady=(5, 0))

        self.replot()
        # self.root.after(config.setup_02_mouse["sample_period_ms"], self.sample_mouse_position)
        self.root.after(config.setup_02_mouse["update_period_ms"], self.update_data)

        # self.root.bind("<Motion>", lambda e: self.process_mouse_movement(e))
        self.root.bind("<Escape>", lambda _: self.close_window())
        self.root.protocol("WM_DELETE_WINDOW", self.close_window)
        self.root.mainloop()

    def update_data(self):
        # Reschedule the function to run again after one second
        # This is an effective way to ensure the function is run periodically
        # inside the Tkinter environment
        self.root.after(config.setup_02_mouse["update_period_ms"], self.update_data)

        # Calculate the average position, velocity and acceleration from the
        # measurements in the last second
        if config.setup_02_mouse["absolute_values"]:
            self.position.push(self.root.winfo_pointerx())
            self.velocity.push(abs(self.position[0] - self.position[1]))
            self.acceleration.push(self.velocity[0] - self.velocity[1])
        else:
            self.position.push(self.root.winfo_pointerx() - self.root.winfo_rootx() - self.width // 2)
            self.velocity.push(self.position[0] - self.position[1])
            self.acceleration.push(self.velocity[0] - self.velocity[1])

        self.replot()

    def move_plot(self):
        t = 1000 * (timer() - self.timer) / config.setup_02_mouse["update_period_ms"]
        self.timer = timer()
        for ax in self.axes:
            a, b = ax.get_xlim()
            ax.set_xlim((a + t, b + t))

    def replot(self):
        # Clear each figure and plot the zero line
        for ax in self.axes:
            ax.clear()
            # if not config.setup_02_mouse["absolute_values"]:
            ax.axhline(y=0, color="w", linestyle="--", linewidth=1)

        # Position
        pos = self.position.get_all_reversed()
        spl = make_interp_spline(self.x, pos, k=2)
        pos_smooth = spl(self.x_smooth)

        self.axes[0].plot(self.x_smooth, pos_smooth, linewidth=2, color=config.plot_colors["position"])
        # self.axes[0].plot(self.x, pos, linewidth=2,
        #                   color=config.plot_colors["position"])

        # position is always positive, set limits to (0, 1.2*max)
        # 1.2 is used to introduce some margin
        pos_max = max([abs(p) for p in pos])
        if pos_max > 0.01:
            if config.setup_02_mouse["absolute_values"]:
                self.axes[0].set_ylim(0, 1.2 * pos_max)
            else:
                self.axes[0].set_ylim(-1.2 * pos_max, 1.2 * pos_max)

        # Velocity
        vel = self.velocity.get_all_reversed()
        spl = make_interp_spline(self.x, vel, k=2)
        vel_smooth = spl(self.x_smooth)

        # self.axes[1].plot(self.x, vel, linewidth=2,
        #                   color=config.plot_colors["velocity"])
        self.axes[1].plot(self.x_smooth, vel_smooth, linewidth=2, color=config.plot_colors["velocity"])

        # velocity can be both positive and negative, ensure that the zero
        # line stays in the middle
        vel_max = max([abs(v) for v in vel])
        if vel_max > 0.01:
            if config.setup_02_mouse["absolute_values"]:
                self.axes[1].set_ylim(0, 1.2 * vel_max)
            else:
                self.axes[1].set_ylim(-1.2 * vel_max, 1.2 * vel_max)

        # Acceleration
        acc = self.acceleration.get_all_reversed()
        spl = make_interp_spline(self.x, acc, k=2)
        acc_smooth = spl(self.x_smooth)

        # self.axes[2].plot(self.x, acc, linewidth=2,
        #                   color=config.plot_colors["acceleration"])
        self.axes[2].plot(self.x_smooth, acc_smooth, linewidth=2, color=config.plot_colors["acceleration"])

        # Same for acceleration
        acc_max = max([abs(a) for a in acc])
        if acc_max > 0.01:
            # if config.setup_02_mouse["absolute_values"]:
            #     self.axes[2].set_ylim(0, 1.2*acc_max)
            # else:
            self.axes[2].set_ylim(-1.2 * acc_max, 1.2 * acc_max)

        # print("Plot", timer() - self.timer)
        self.timer = timer()
        for i in range(1, self.submoves):
            self.root.after(i * config.setup_02_mouse["update_period_ms"] // self.submoves, self.move_plot)

    def sample_mouse_position(self):
        # Add the mouse x data to the measurement history
        self.position_data.append(self.root.winfo_pointerx())

        # Reschedule the function to run again after one second
        # This is an effective way to ensure the function is run periodically
        # inside the Tkinter environment
        self.root.after(config.setup_02_mouse["sample_period_ms"], self.sample_mouse_position)

    def close_window(self):
        self.root.quit()
        self.root.destroy()


if __name__ == "__main__":
    app = App()
    app.run()
