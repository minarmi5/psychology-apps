# Psychology Apps

Clone this repository with

```sh
git clone git@gitlab.fel.cvut.cz:minarmi5/psychology-apps.git
```

## Table of contents

[1. Setup and configuration](#1-setup-and-configuration)

[2. Apps](#2-apps)

&ensp;[2.1 Target](#21-target)

&ensp;[2.2 Mouse](#22-mouse)

&ensp;[2.3 Math](#23-math)

[3. Authors](#3-authors)

## 1. Setup and configuration

This project was implemented and tested using **Python 3.8**.
The graphical interface uses **tkinter** module, which is included in the Python 3 core.

For the second app to work, **matplotlib** and **scipy** packages are needed.
Please install them by running

```bash
pip3 install -r requirements.txt
```

All configuration options are found in `config.py` (colors, window size etc.).
This file is used for all apps to ensure the design is consistent and the user can easily change it to meet his needs.

## 2. Apps

After running any of the apps a window is opened.
All apps can be exited using the **esc** key, as well as by closing the window.

The size of the window is controlled by the configuration file `config.py`.
They can either be set to automatically occupy maximum screen size (set **fullscreen: True** in `config.py`) or have a fixed size (**fullscreen: False** and set the corresponding **width** and **height**).

<div style="page-break-after: always;"></div>

### 2.1 Target

The goal in the first app is reaching a **target** (a moving dot on a screen), with the mouse **cursor**.
However, the mouse movements are altered or inverted: up to down, left to right and possibly with a little angular deviation.
To start the app, run
```bash
python3 01_target.py
```

User inputs his name in the entry box and presses **Start** - this brings up the cursor and starts the experiment.

#### 2.1.1 Target - one trial

A trial is started after **spacebar** is pressed - this moves the target to a random location and starts the timer.
Upon reaching the target, the timer is stopped and the response time is saved.
The user is requested to press the spacebar again to start a new trial.
After all trials have been completed, the result is saved into a resulting csv file.
The number of the trials is specified in `config.py` in
```python
setup_01_target["trials"]
```

#### 2.1.2 Target - mouse and cursor behavior

Initial position of the mouse shown on the screen is random.
In each direction, the movement can be inverted, i.e. mouse moving upwards results in the cursor moving downwards, moving mouse left moves cursor right.
To increase complexity, it is possible to add a constant angular deviation to all shown movements, e.g. all deviated by 30 degrees clockwise.

The behavior can be configured in the `config.py` file with **invert_x**, which flips the left-right movement, **invert_y**, which flips up-down and **angular_offset_deg**, which adds the angular component.

```python
setup_01_target["cursor"] = 
{
    "invert_x": False,
    "invert_y": True,
    "angular_offset_deg": 30
}
```

<div style="page-break-after: always;"></div>

The appearance of the cursor (color and size) can be also modified in the `config.py` file

```python
cursor = 
{
    "color": 
    {
        "fill": "#3465a4", 
        "outline": "#3465a4"
    }, 
    "size": 20
}
```

#### 2.1.3 Target - target behavior

The initial position of the target is random, however, it is chosen such that the reachability of the target given the parameters (mainly **angular_offset_deg**) is guaranteed.
The target is stationary and its appearance (color and size) can be modified in the `config.py` file

```python
target = 
{
    "color": 
    {
        "fill": "#3465a4", 
        "outline": "#3465a4"
    }, 
    "size": 20
}
```

#### 2.1.4 Target - output

After all trials for one user are completed, a csv file with the name of the participant is saved to `config.setup_01_target["result"]`.

| Participant | Trial | Response time [s] |
| ----------- | ----- | ----------------- |
| john        | 1     | 9.329             |
| john        | 2     | 4.155             |
| john        | 3     | 3.164             |
| john        | 4     | 4.103             |
| john        | 5     | 5.856             |

<div style="page-break-after: always;"></div>

### 2.2 Mouse

In the second one the are three graphs, without any axis labels, of the X position of the mouse, its velocity and acceleration.

To start the app, run
```bash
python3 02_mouse.py
```

The configuration can be found again in the 'config.py' file, namely:
- **data_len** - length of the sliding window (number of datapoints in the graph, old data is deleted to make room for new data points)
- **plot_period_ms** - period of updating the graphs in the app
- **sample_period_ms** - period of sampling the mouse X position

In the `config.py` file, we can switch between **relative** measurement of the position and velocity (relative to the center of the window) and **absolute** measurement (from the upper left of the window with absolute values of the velocity) with the switch **"absolute_values": True/False**.
The acceleration is always relative (positive for speeding up, negative for slowing down).

<div style="page-break-after: always;"></div>

### 2.3 Math

The third app deals with changing the display and complexity of a simple mathematical operation, e.g. `17 + 62 = `.
The user's task is to input **the first digit** of the result of a mathematical sum or difference of two 2-digit numbers. e.g.:
```
task: 33 + 16
correct result: 4 (first digit of 49)
```

The participant **moves the mouse with one hand** and **answers the maths with the other**.

The appearance changes with
- the velocity of the mouse (the faster the mouse movement, the higher contrast between background and the math task)
- the acceleration of the mouse (the higher the acceleration, the more jittery the math task gets, i.e. it moves around more)

The complexity changes with
- average mouse x position in the last trial (**+** if in the **right half** of the window, **-** if in the **left half**)
- average mouse y position in the last trial (the second digit of both number is less than 5 if in the **upper half** of the window)
  

The **speed of the mouse** should be **above a threshold** (specified in `config.setup_03_math["minimal_mouse_velocity"]) for the trial to go on as we want to ensure that the participants keep moving the mouse.
If minimum speed falls below, the trial is not counted (is reset) and its associated data is discarded.

The position of the mouse is measured periodically with the period specified by `config.setup_03_math["update_period_ms"]`.
The velocity and acceleration are computed from averaged position of the mouse.
The averages are computed in a sliding window with a length defined in `config.setup_03_math["averaging_window_len"]`.

Upon start, the participant is asked to input his name and press enter to start the experiment.
Before first trial, a screen is shown asking participant to start moving the mouse.
A red square is displayed if the mouse speed is below the minimum threshold and a green square if it is above.

#### 2.3.1 Math - trial

On every trial, simply display the same red square if the mouse speed
is below the minimum threshold and a green square if it is above, instead
of the exact number of the speed

Next trial starts automatically when the participant presses one digit on the keyboard, so as not to interrupt the mouse movement.

#### 2.3.1 Math - output

The results of the experiment are saved to two files in folder `config.setup_03_math["results"]` for each participant.

`<participant_name>.csv` contains information about the trials and their results

| Participant | Trial | Response time [s]  | Math task | Correct answer | Answer | Correct |
| ----------- | ----- | ------------------ | --------- | -------------- | ------ | ------- |
| john        | 1     | 3.3419386779996785 | 60 + 35   | 9              | 9      | 1       |
| john        | 2     | 1.4500757380001232 | 30 + 15   | 4              | 4      | 1       |
| john        | 3     | 1.469859212999836  | 60 + 35   | 9              | 9      | 1       |
| john        | 4     | 2.4144541490004485 | 58 + 23   | 8              | 8      | 1       |
| john        | 5     | 2.249703853999563  | 63 + 29   | 9              | 5      | 0       |

`<participant_name>.mouse.csv` contains measured mouse position, velocity and acceleration, sampled at `config.setup_03_math["update_period_ms"]`.

| Participant | Trial | Time [s]             | x [px] | y [px] | Velocity [px/s]    | Acceleration [px/s^2] |
| ----------- | ----- | -------------------- | ------ | ------ | ------------------ | --------------------- |
| john        | 1     | 0.014000539999869943 | 874    | 1681   | 2678.5207638544853 | 878.9230564827299     |
| john        | 1     | 0.06466763299977174  | 1056   | 1625   | 2770.1822171471104 | 648.1115132546486     |
| john        | 1     | 0.11569666899958975  | 1076   | 1562   | 2584.9182155119042 | 925.0369681824791     |
| john        | 1     | 0.16657746900000348  | 1057   | 1540   | 2268.2318582543794 | 1056.459323804798     |
| john        | 1     | 0.21727088699935848  | 1044   | 1553   | 1949.9750982822322 | 1058.0297265194206    |

## 3. Authors

Michal Minařík (minarmi5@fel.cvut.cz)