class CircularBuffer:
    def __init__(self, capacity, default=None):
        self.buffer = [default for _ in range(capacity)]
        self.head = -1
        self.capacity = capacity

    def push(self, value):
        if self.head == self.capacity - 1:
            self.head = 0
        else:
            self.head += 1

        self.buffer[self.head] = value

    def _get_slice(self, start, stop):
        start = (self.head - start) % self.capacity
        stop = (start - stop + 1) % self.capacity
        to_return = []
        if start == stop:
            return self.buffer[start]
        elif start > stop:
            for i in range(start, stop - 1, -1):
                to_return.append(self.buffer[i])
            return to_return

        else:
            for i in range(start, -1, -1):
                to_return.append(self.buffer[i])
            for i in range(self.capacity - 1, stop - 1, -1):
                to_return.append(self.buffer[i])
            return to_return

    def __getitem__(self, key):
        if isinstance(key, slice):
            return self._get_slice(key.start, key.stop)
        return self.buffer[(self.head - key) % self.capacity]

    def get_all(self):
        return self[0 : self.capacity]

    def get_all_reversed(self):
        to_return = []
        for i in range(self.head + 1, self.capacity):
            to_return.append(self.buffer[i])
        for i in range(0, self.head + 1):
            to_return.append(self.buffer[i])
        return to_return
