#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
"""

import os
import csv
import math
import random
import tkinter as tk
import tkinter.font as font

from timeit import default_timer as timer

import config


class User:
    def __init__(self, name=""):
        self.name = name

        # List containing (id, response_time) for each trial
        self.response_times = []

    def save_response_times(self):
        # Ensure the results folder exists
        if not os.path.isdir(config.setup_01_target["results"]):
            os.mkdir(config.setup_01_target["results"])

        # Save the response times
        with open(f"{config.setup_01_target['results']}/{self.name}.csv", mode="w") as f:
            csv_writer = csv.writer(f)
            # Write header
            csv_writer.writerow(["Participant", "Trial", "Response time [s]"])

            for row in self.response_times:
                # Add the Participant name to each row
                csv_writer.writerow([self.name] + row)


class Cursor:
    def __init__(self, canvas, x, y):
        self.canvas = canvas
        # Tkinter object
        # create_oval(x0, y0, x1, y1, option, ...)
        # https://anzeljg.github.io/rin2/book2/2405/docs/tkinter/create_oval.html
        self.tk_id = self.canvas.create_oval(
            x - config.cursor["size"] // 2,
            y - config.cursor["size"] // 2,
            x + config.cursor["size"] // 2,
            y + config.cursor["size"] // 2,
            **config.cursor["color"],
        )

        # Mouse moves left, cursor moves right
        self.invert_x = config.setup_01_target["cursor"]["invert_x"]
        # Mouse moves up, cursor moves down
        self.invert_y = config.setup_01_target["cursor"]["invert_y"]

        # Mouse moves straight up, cursor moves with angular offset
        self.angular_offset_deg = config.setup_01_target["cursor"]["angular_offset_deg"]

    def move_with_mouse(self, mouse_dx, mouse_dy):
        """
        Moves the cursor based on the mouse movement
        """
        # Invert if invert_x or invert_y is set, leave the value otherwise
        if self.invert_x:
            mouse_dx *= -1
        if self.invert_y:
            mouse_dy *= -1

        # Rotate by the given angle
        th = math.radians(self.angular_offset_deg)

        dx = math.cos(th) * mouse_dx - math.sin(th) * mouse_dy
        dy = math.sin(th) * mouse_dx + math.cos(th) * mouse_dy

        self.move_by(dx, dy)

    def move_to(self, x, y):
        self.canvas.coords(
            self.tk_id,
            x - config.cursor["size"] // 2,
            y - config.cursor["size"] // 2,
            x + config.cursor["size"] // 2,
            y + config.cursor["size"] // 2,
        )

    def move_by(self, dx, dy):
        x0, y0, x1, y1 = self.canvas.coords(self.tk_id)
        self.canvas.coords(self.tk_id, x0 + dx, y0 + dy, x1 + dx, y1 + dy)

    def get_bounding_box(self):
        return self.canvas.coords(self.tk_id)

    def get_center(self):
        x0, y0, x1, y1 = self.canvas.coords(self.tk_id)
        return ((x0 + x1) // 2, (y0 + y1) // 2)

    def show(self):
        self.canvas.itemconfigure(self.tk_id, state="normal")

    def hide(self):
        self.canvas.itemconfigure(self.tk_id, state="hidden")


class Target:
    def __init__(self, canvas, x, y):
        self.canvas = canvas
        # Tkinter object
        # create_oval(x0, y0, x1, y1, option, ...)
        # https://anzeljg.github.io/rin2/book2/2405/docs/tkinter/create_oval.html
        self.tk_id = self.canvas.create_oval(
            x - config.target["size"] // 2,
            y - config.target["size"] // 2,
            x + config.target["size"] // 2,
            y + config.target["size"] // 2,
            **config.target["color"],
        )

    def move_to(self, x, y):
        self.canvas.coords(
            self.tk_id,
            x - config.target["size"] // 2,
            y - config.target["size"] // 2,
            x + config.target["size"] // 2,
            y + config.target["size"] // 2,
        )

    def get_bounding_box(self):
        return self.canvas.coords(self.tk_id)

    def show(self):
        self.canvas.itemconfigure(self.tk_id, state="normal")

    def hide(self):
        self.canvas.itemconfigure(self.tk_id, state="hidden")


class App:
    def __init__(self):
        self.last_mouse_pos = None

        self.experiment_running = False
        self.trial_running = False
        self.trial_id = 0

        # Class representing the current user
        # (holds name and response times)
        self.current_user = User()

        # Used to measure time required to reach the target
        self.trial_start = None

    def run(self):
        # Create the window environment
        self.root = tk.Tk(className="01_target")
        self.root["background"] = config.window["color"]["background"]
        # .geometry requires window size as a string "WxH"
        if config.window["size"]["fullscreen"]:
            self.width = self.root.winfo_screenwidth()
            self.height = self.root.winfo_screenheight()
        else:
            self.width = config.window["size"]["width"]
            self.height = config.window["size"]["height"]

        self.root.geometry(f"{self.width}x{self.height}")
        # self.root.attributes("-fullscreen", True)
        # Create the main canvas
        self.canvas = tk.Canvas(self.root, bg=config.window["color"]["background"], highlightthickness=0)
        self.canvas.pack(fill="both", expand=True)

        # Prepare entry box
        my_font = font.Font(font=config.info_text["font"])
        self.name_entry = tk.Entry(self.root)
        self.name_entry_window = self.canvas.create_window(
            self.width // 2, self.height // 2 - 40, window=self.name_entry
        )
        self.name_entry["font"] = my_font
        self.name_entry.focus()

        # Prepare start button
        self.start_button = tk.Button(
            self.root, text="Start", bd=0, **config.button_colors, command=self.start_experiment
        )

        self.start_button["font"] = my_font
        self.start_button.place(x=self.width // 2, y=self.height // 2, anchor=tk.CENTER)

        # Prepare trial counter
        self.info_text = tk.Text(self.root, width=39, pady=10, height=1, **config.info_text)

        self.info_text.place(x=2, y=0, anchor=tk.NW)
        self.info_text.insert("1.0", "Enter your name and press 'Start'")

        self.trial_id_text = tk.Text(self.root, width=12, pady=5, height=1, **config.info_text)

        self.trial_id_text.place(x=2, y=35, anchor=tk.NW)

        self.root.bind("<Motion>", self.process_mouse_movement)
        self.root.bind("<space>", lambda _: self.start_trial())
        self.root.bind("<Escape>", lambda _: self.close_window())
        self.root.protocol("WM_DELETE_WINDOW", self.close_window)
        self.root.mainloop()

    def start_experiment(self):
        name = self.name_entry.get()

        # Do not proceed when the name is empty
        if name == "":
            return

        self.current_user.name = name

        self.experiment_running = True

        # Hide the entry box
        self.canvas.delete(self.name_entry_window)

        # Hide the start button
        self.start_button.place_forget()

        # Create cursor and target objects
        self.cursor = Cursor(canvas=self.canvas, x=self.width // 2, y=self.height // 2)

        self.target = Target(canvas=self.canvas, x=self.width // 2, y=self.height // 2 - 100)

        self.cursor.hide()
        self.target.hide()
        # Hide the mouse cursor, the app cursor with modified movement will
        # be used instead
        self.root.config(cursor="none")

        self.info_text.delete("1.0", tk.END)
        self.info_text.insert("1.0", "Press <spacebar> to start a next trial")

    def end_experiment(self):
        # Save the response times to csv
        self.current_user.save_response_times()
        # End the experiment so that mouse movement does not move cursor
        self.experiment_running = False

        # Show the mouse cursor
        self.root.config(cursor="arrow")

        self.trial_id_text.delete("1.0", tk.END)
        self.trial_id_text.insert("1.0", "Done!")

        self.info_text.delete("1.0", tk.END)
        self.info_text.insert("1.0", "Press <esc> or close the window to exit")

    def start_trial(self):
        if not (self.experiment_running and not self.trial_running):
            return

        self.trial_id += 1
        self.trial_id_text.delete("1.0", tk.END)
        self.trial_id_text.insert("1.0", f"Trial {self.trial_id}/{config.setup_01_target['trials']}")

        self.info_text.delete("1.0", tk.END)

        self.trial_running = True

        # Move the target

        # Ensure the target is reachable - by placing it into a circle
        # centered in the middle and with radius height/2 (assuming
        # height < width), the target is always reachable, no matter the angular offset
        w = self.root.winfo_width()
        h = self.root.winfo_height()

        # Introduce some more margin just to be sure
        x_min = int(0.1 * w)
        x_max = int(0.9 * w)
        y_min = int(0.1 * h)
        y_max = int(0.9 * h)

        x = random.randint(x_min, x_max)
        y = random.randint(y_min, y_max)
        while ((x - w // 2) ** 2 + (y - h // 2) ** 2) > (h // 2) ** 2:
            x = random.randint(x_min, x_max)
            y = random.randint(y_min, y_max)

        self.target.move_to(x, y)
        self.target.show()
        self.cursor.show()

        self.trial_start = timer()

    def end_trial(self, response_time):
        if not (self.experiment_running and self.trial_running):
            return
        self.trial_running = False
        self.current_user.response_times.append([self.trial_id, response_time])
        self.target.hide()
        self.cursor.hide()

        if self.trial_id < config.setup_01_target["trials"]:
            self.info_text.insert("1.0", "Press <spacebar> to start a next trial")

    def process_mouse_movement(self, e):
        # If the experiment is not running, move the cursor normally
        if not self.experiment_running:
            return

        # Get mouse pointer position
        mouse_x = e.x
        mouse_y = e.y

        if self.last_mouse_pos is not None:
            self.cursor.move_with_mouse(mouse_x - self.last_mouse_pos.x, mouse_y - self.last_mouse_pos.y)

        self.last_mouse_pos = e

        # Do not check reaching the target when trial is not running
        if not self.trial_running:
            return

        # Get cursor center position
        x, y = self.cursor.get_center()

        # Get target bounding box
        x0, y0, x1, y1 = self.target.get_bounding_box()

        # Check whether the target was reached
        if x >= x0 and x <= x1 and y >= y0 and y <= y1:
            # If so, save the reaction time and move the target
            trial_end = timer()
            duration = trial_end - self.trial_start
            self.end_trial(duration)
            print(f"Time to reach the target was {duration:.4f} s")

            if self.trial_id == config.setup_01_target["trials"]:
                self.end_experiment()

    def _move_target_to_mouse(self, e):
        self.target.move_to(e.x, e.y)

    def close_window(self):
        self.root.quit()
        self.root.destroy()


if __name__ == "__main__":
    app = App()
    app.run()
