#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
"""

###############################################################################################
# COMMON SETUP
###############################################################################################
window = {
    "size": {
        "fullscreen": False,
        "width": 1500,
        "height": 1000,
    },  # "fullscreen": True might cause issues when using multiple screens
    "color": {"background": "#000000"},
}

# Text in upper left corner showing trial number and instructions
info_text = {
    "font": ("Verdana", 14),   # (family, size)
    "background": "#000000",
    "foreground": "#d3d7cf",
    "bd": 0,
    "highlightthickness": 0,
}

# Start button
button_colors = {
    "activebackground": "#3465a4",
    "background": "#3465a4",
    "activeforeground": "#000000",
    "foreground": "#000000",
    "highlightthickness": 0,
    "highlightbackground": "#000000",
}

###############################################################################################
# 01_TARGET
###############################################################################################

setup_01_target = {
    "results": "results/01_target/",
    "trials": 5,
    "cursor": {"invert_x": False, "invert_y": True, "angular_offset_deg": 0},
}
setup_01_target_NL = {
    "results": "results/01_target_NL/",
    "trials": 50,
    "cursor": {"invert_x": False, "invert_y": False, "angular_offset_deg": 0},
}

cursor = {"color": {"fill": "#3465a4", "outline": "#3465a4"}, "size": 20}

target = {"color": {"fill": "#4e9a06", "outline": "#4e9a06"}, "size": 40}

###############################################################################################
# 02_MOUSE
###############################################################################################

setup_02_mouse = {"data_len": 100, "absolute_values": False, "update_period_ms": 200}

plot_colors = {
    "background": "#262b2b",
    "spines": "#d3d7cf",
    "tickers": "#d3d7cf",
    "position": "#3465a4",
    "velocity": "#4e9a06",
    "acceleration": "#dd4814",
}

###############################################################################################
# 03_MATH
###############################################################################################

setup_03_math = {
    "results": "results/03_math/",
    "trials": 5,
    "text": {
        "font": ("Verdana", 120, "bold"),  # (family, size, style)
        "color": {
            "right": "#8ad355",
            "wrong": "#e04854",
            "invalid": "#e6bb29",
            # Use the same value for all components
            # in order to make the interpolation between
            # low and high contrast easy
            "low_contrast": 1,
            "high_contrast": 100,
        },
    },
    "minimal_mouse_velocity": 200,
    "update_period_ms": 50,
    "averaging_window_len": 5,
}
